--
-- NoPoke by KattaLord
-- Event Handling
--

local active = true;
local whitelist = "NoPokeAllowedPokers.txt"

local MenuIDs = {
	MENU_ID_CLIENT_WHITELIST  = 1,
	MENU_ID_GLOBAL_ENABLE  = 2,
	MENU_ID_GLOBAL_WHITELIST  = 3
}

-- Will store factor to add to menuID to calculate the real menuID used in the TeamSpeak client (to support menus from multiple Lua modules)
-- Add this value to above menuID when passing the ID to setPluginMenuEnabled. See demo.lua for an example.
local moduleMenuItemID = 239

local function onClientPokeEvent(serverConnectionHandlerID, pokerID, pokerName, message, ffIgnored)
    local retVal = 0
	if active then
		local pokerUID, error = ts3.getClientVariableAsString(serverConnectionHandlerID, pokerID, ts3defs.ClientProperties.CLIENT_UNIQUE_IDENTIFIER)	
		retVal = 1
		if error == ts3errors.ERROR_ok then
			local server, error = ts3.getServerVariableAsString(serverConnectionHandlerID, ts3defs.VirtualServerProperties.VIRTUALSERVER_NAME)
			if error ~= 0 then
				server = "ID: " .. serverConnectionHandlerID
			end
			local pokeNotification = "[NoPoke] Got a poke from '" ..pokerName.. "' ("..pokerUID..") with following message: '" .. message .. "' on server '" .. server .. "'"
			if message == "" then
				pokeNotification = "[NoPoke] Got a poke from '" ..pokerName.. "' ("..pokerUID..") with no message on server '" .. server .. "'"
			end
			ts3.printMessage(serverConnectionHandlerID, pokeNotification, ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
			print(pokeNotification)
			-- Check if poker is whitelisted
			local f = assert(io.open(whitelist, "r"))
			if f~=nil then 
				for line in io.lines(whitelist) do
					if line == pokerUID then 
						retVal = 0
						print("UID '"..line.."' is whitelisted")
						break
					end
				end
				f:close()
			end
			--print ("RetVal: "..retVal)
			if retVal == 1 then
				local error = ts3.requestSendPrivateTextMsg(serverConnectionHandlerID, "This User has disabled pokes. Send a private message instead", pokerID)
				if error ~= 0 then
					print(error)
				end
			end
		end
	end
	return retVal
end

local function onPluginCommandEvent(serverConnectionHandlerID, pluginName, pluginCommand)
	print("[NoPoke]: Command was triggered: " .. serverConnectionHandlerID .. " " .. pluginName .. " " .. pluginCommand)
end

--
-- Called when a plugin menu item (see ts3plugin_initMenus) is triggered. Optional function, when not using plugin menus, do not implement this.
--
-- Parameters:
--  serverConnectionHandlerID: ID of the current server tab
--  type: Type of the menu (ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_CHANNEL, ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_CLIENT or ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_GLOBAL)
--  menuItemID: Id used when creating the menu item
--  selectedItemID: Channel or Client ID in the case of PLUGIN_MENU_TYPE_CHANNEL and PLUGIN_MENU_TYPE_CLIENT. 0 for PLUGIN_MENU_TYPE_GLOBAL.
--
local function onMenuItemEvent(serverConnectionHandlerID, menuType, menuItemID, selectedItemID)
	if menuType == ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_GLOBAL then
		if menuItemID == MenuIDs.MENU_ID_GLOBAL_ENABLE then
			if active == true then
				--ts3.printMessage(serverConnectionHandlerID, "[NoPoke] Deactivated!", ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
				ts3.printMessageToCurrentTab("[NoPoke] Deactivated!")
				active = false
			else
				--ts3.printMessage(serverConnectionHandlerID, "[NoPoke] Activated!", ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
				ts3.printMessageToCurrentTab("[NoPoke] Activated!")
				active = true
			end
		end
		if menuItemID == MenuIDs.MENU_ID_GLOBAL_WHITELIST then
			--ts3.printMessage(serverConnectionHandlerID, "Allowed Pokers:", ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
			ts3.printMessageToCurrentTab("Allowed Pokers:")
	
			local f = assert(io.open(whitelist, "r"))
			local allowedPokers = f:read("*all")
			f:close()
			--ts3.printMessage(serverConnectionHandlerID, allowedPokers:gsub("0\n", ""), ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
			ts3.printMessageToCurrentTab(allowedPokers:gsub("0\n", ""))
		end
		return 0
	end
	if menuType == ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_CLIENT then
		if menuItemID == MenuIDs.MENU_ID_CLIENT_WHITELIST then
			local file = assert(io.open(whitelist, "r"))	
			local allowedPokers = file:read("*all")
			file:close()
			
			local selectedUID, error = ts3.getClientVariableAsString(serverConnectionHandlerID, selectedItemID, ts3defs.ClientProperties.CLIENT_UNIQUE_IDENTIFIER)
			if error == ts3errors.ERROR_ok then
				local whitelisted = false
				
				file = assert(io.open(whitelist, "r"))
				for line in io.lines(whitelist) do
					if line == selectedUID then 
						whitelisted = true
						break
					end
				end
				file:close()
				
				if whitelisted then
					local removed = "[NoPoke] Removing "..selectedUID.." from whitelist"
					print(removed)
					--ts3.printMessage(serverConnectionHandlerID, removed, ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
					ts3.printMessageToCurrentTab(removed)
					
					local allowedPokers = ""
					file = assert(io.open(whitelist, "r"))
					for line in io.lines(whitelist) do
						if line ~= selectedUID then 
							if allowedPokers ~= "" then allowedPokers = allowedPokers .. "\n" end
							allowedPokers = allowedPokers .. line
						end
					end
					file:close()
					file = assert(io.open(whitelist, "w+"))
					local new, count = allowedPokers:gsub("\n\n", "\n")
					file:write(new)
					file:close()
				else
					local added = "[NoPoke] Adding "..selectedUID.." to whitelist"
					print(added)
					--ts3.printMessage(serverConnectionHandlerID, added, ts3defs.TextMessageTargetMode.TextMessageTarget_SERVER)
					ts3.printMessageToCurrentTab(added)
					file = assert(io.open(whitelist, "a+"))	
					file:write("\n"..selectedUID)
					file:close()
				end
			end
		end
		return 0
	end
end

nopoke_events = {
	MenuIDs = MenuIDs,
	moduleMenuItemID = moduleMenuItemID,
	onClientPokeEvent = onClientPokeEvent,
	onPluginCommandEvent = onPluginCommandEvent,
	onMenuItemEvent = onMenuItemEvent
}
