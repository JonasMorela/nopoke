--
-- NoPoke by KattaLord
-- Functions
--
require("ts3defs")
require("ts3errors")

--
-- Call these function from the TeamSpeak 3 client console via: /lua run nopoke.<function>
-- Note the serverConnectionHandlerID of the current server is always passed.
--
-- You might want to pass the "-console" option when starting the TeamSpeak 3 client to get a console where a lot
-- of plugin related debug output will appear.
--


local function listAllowedPokers()
	ts3.printMessageToCurrentTab("Allowed Pokers:")
	
	local f = assert(io.open("NoPokeAllowedPokers.txt", "r"))
    local allowedPokers = f:read("*all")
    f:close()
	ts3.printMessageToCurrentTab(allowedPokers)
end

-- Run with "/lua run nopoke.sendCommand <command>"
local function sendCommand(serverConnectionHandlerID, command)
	--[[
	Target Mode: 
	0 = send to all clients in current channel (targetIDs ignored)
	1 = send to all clients on server (targetIDs ignored)
	2 = send to all given client targetIDs
	3 = send to all subscribed clients in current channel (targetIDs ignored)
	]]--
	local targetMode = 0
	local targetIDs = {}
	local error = ts3.sendPluginCommand(serverConnectionHandlerID, command, targetMode, targetIDs)
	if error ~= ts3errors.ERROR_ok then
		print("Error sending plugin command: " .. error)
	end
	-- Monitor onPluginCommandEvent to see the incoming event
end


-- Run with "/lua run nopoke.setPluginMenuEnabled <menuID> <1|0>"
local function setPluginMenuEnabled(serverConnectionHandlerID, menuID, enabled)
	-- To get the correct menuID, add nopoke_events.moduleMenuItemID to the given menuID (MENU_ID_CLIENT_1 etc.)
	print("setPluginMenuEnabled: " .. menuID .. " " .. enabled .. " mult: " .. nopoke_events.moduleMenuItemID)
	ts3.setPluginMenuEnabled(nopoke_events.moduleMenuItemID + menuID, enabled)
end



nopoke = {
	listAllowedPokers = listAllowedPokers,
	sendCommand = sendCommand,
	setPluginMenuEnabled = setPluginMenuEnabled
}
