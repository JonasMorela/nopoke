# NoPoke

A simple Lua TS3 plugin to allow blocking of pokes

## Features

- Blocks incoming pokes
- Plugin can be toggled on and off
- Whitelist to allow single users to poke

## Installation
1. Install the [TS3 Lua Plugin](https://www.myteamspeak.com/addons/1ea680fd-dfd2-49ef-a259-74d27593b867) and enable it in your TeamSpeak settings.
2. Download this repository and place it inside the folder of the lua plugin (Default Path: `%appdata%\TS3Client\plugins\lua_plugin`).
3. Create the whitelist file inside of your TeamSpeak installation folder. The name needs to be `NoPokeAllowedPokers.txt`.
4. Start TeamSpeak and enable NoPoke in the settings of the lua plugin. (Also, you might want to disable the test plugin if it is enabled.)

## Future Versions

I will probably not update this version of the plugin. I started to work on a native C plugin some time ago, but have not continued to develop it.