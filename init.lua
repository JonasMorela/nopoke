--
-- NoPoke by KattaLord
-- Plugin Initialization
--

require("ts3init")            -- Required for ts3RegisterModule
print("[NoPoke] Loading Events...")
require("nopoke/events")  -- Forwarded TeamSpeak 3 callbacks
print("[NoPoke] Events loaded!")
print("[NoPoke] Loading functions...")
require("nopoke/nopoke")
print("[NoPoke] Functions loaded!")

MODULE_NAME = "NoPoke"
VERSION = "0.3"

--
-- Initialize menus. Optional function, if not using menus do not implement this.
-- This function is called automatically by the TeamSpeak client.
--
local function createMenus(moduleMenuItemID)
	nopoke_events.moduleMenuItemID = moduleMenuItemID

	return {
		{ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_CLIENT,  nopoke_events.MenuIDs.MENU_ID_CLIENT_WHITELIST,  "NoPoke: Add to / Remove from whitelist",  ""},
		{ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_GLOBAL,  nopoke_events.MenuIDs.MENU_ID_GLOBAL_ENABLE,  "NoPoke: Toggle On/Off",  ""},
		{ts3defs.PluginMenuType.PLUGIN_MENU_TYPE_GLOBAL,  nopoke_events.MenuIDs.MENU_ID_GLOBAL_WHITELIST,  "NoPoke: List whitelisted UIDs",  ""}
	}
end

local registeredEvents = {
	createMenus = createMenus,
	onClientPokeEvent = nopoke_events.onClientPokeEvent,
	onPluginCommandEvent = nopoke_events.onPluginCommandEvent,
	onMenuItemEvent = nopoke_events.onMenuItemEvent
}

-- Register your callback functions with a unique module name.
ts3RegisterModule(MODULE_NAME, registeredEvents)
print("[NoPoke] Successfully registered!")
